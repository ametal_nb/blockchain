const express = require('express')
const app = express()
const port = 3000
const fs = require('fs')
var router = express.Router()
let jsonFile = fs.readFileSync('../build/contracts/Simple.json')
var abiArray = JSON.parse(jsonFile).abi

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded());

app.use(bodyParser.json());
const Web3 = require('web3');

let web3 = new Web3('ws://localhost:7545');
//console.log('abi >> ',abiArray)
const contractAddress = '0x84652A228dAb86FB6AE4dc390EE971Db5861cBE2'
let contract = new web3.eth.Contract(abiArray, contractAddress);



app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => {
    console.log('json >> ',contract)
    res.send('Hello World!')
})


app.post('/save',(req,res) => {
    console.log('req >> ',req.body.lotties)
    //console.log('method : ',contract.methods.deposit())
    // contract.methods.set(req.body.details.fields[0].price).call({

    //      from: req.body.details.fields[0].account,
    //     // value: req.body.details.fields[0].price
    //     //testValue: 0
    // })      
    // .then(function(result) {
    //     console.log(result);
    //     // alert(JSON.stringify(result));
    // });

    // contract.methods.set(req.body.details.fields[0].price).send({
    //     from: req.body.details.fields[0].account
    // }, function(error, result){
    //     console.log('result >> ',result)
    // })

    // contract.methods.get().call({
    //     from: req.body.lotties.fields[0].account,
    //   }).then(function(result) {
    //         console.log(result);
    //         // alert(JSON.stringify(result));
    //         // document.getElementById("withdrawAmount").value = result;
    // });
    
})

app.get('/getAccounts',(req,res) => {
    web3.eth.getAccounts().then(function(accounts) {
        res.json(accounts)  
      }) 
})

app.post('/generate',(req,res) => {
    let lotto = getRandomInt(0,100)
    res.json({ number: lotto})
})


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

app.post('/deposit',(req,res) => {
    console.log('details newCampaign >>> ',contract.methods,' goal : ',req.body.lotties.fields[0].age, ' buyer : ',req.body.lotties.fields[0].account)
   //req.body.lotties.fields[0].account, req.body.lotties.fields[0].age
    contract.methods.deposit(req.body.lotties.fields[0].id, req.body.lotties.fields[0].lottoNumber, req.body.lotties.fields[0].price,req.body.lotties.fields[0].round , req.body.lotties.fields[0].account).send({
            from: req.body.lotties.fields[0].account,
            value: req.body.lotties.fields[0].price * 1000000000000000000
        }, function(error, result){
            console.log('result >> ',result)
        })

        // var event = contract.LogCampaign(function(error, result) {
        //     if (!error)
        //         console.log(result);
        // });

        
        // contract.getPastEvents('LogDepositMade', {
        //    // filter: {id: id},  
        //     fromBlock: 0,
        //     toBlock: 'latest'
        // }, (error, events) => { 
      
        //     if (!error){
        //       var obj=JSON.parse(JSON.stringify(events));
        //       var array = Object.keys(obj)
      
        //       console.log("returned values",obj[array[0]].returnValues);
      
        //     }
        //     else {
        //       console.log(error)
        //     }})
})


app.get('/viewLottoNumber',(req,res) => {
    console.log('details getInstructors >>> ')
    // contract.methods.viewLottoNumber(req.body.lotties.fields[0].id).call({
    //         from: req.body.lotties.fields[0].account,
    //     }, function(error, result){
    //         console.log('result >> ',result)

    //         // var open = web3.eth.getTransaction(result).then(function(result) {
    //         //     console.log('get tran >> ',result);
    //         // })
    //     })

    contractInstance = new web3.eth.Contract(abiArray).at(contractAddress);


        
            contractInstance.viewLottoNumber(function(error, result){
                //document.getElementById("output").innerHTML = result;
                console.log('data result >> ',result)
            });
        

    })


    app.get('/depositsBalance',(req,res) => {
        console.log('details depositsBalance >>> ')
        contract.methods.depositsBalance().call({
                from: req.body.lotties.fields[0].account,
            }, function(error, result){
               // console.log('result >> ',result)
                var open = web3.eth.getTransaction(result).then(function(result) {
                    console.log('get tran >> ',result);
                })
            })
        })

     app.post('/withdraw',(req,res) => {
         console.log('details withdraw >>> ')
         contract.methods.withdraw(req.body.lotties.fields[0].price* 1000000000000000000).send({
                from: req.body.lotties.fields[0].account,
            }, function(error, result){
                console.log('result >> ',result)
         })
     })
    

app.listen(port, () => console.log(`Lottery_Frontend app listening on port ${port}!`))