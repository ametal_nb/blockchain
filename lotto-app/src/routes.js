import lotto from './components/lotto.vue'
import PageNotFound from './components/PageNotFound.vue'
// import Inventory from './components/Inventory.vue'
// import InventoryEdit from './components/InventoryEdit.vue'
// import Navigation from './components/Navigation.vue'
// import NavigationEdit from './components/NavigationEdit.vue'
// import Tools from './components/Tools.vue'

export default {
    mode: 'history',
    routes: [
        // Redirect to home to easily (lazily) fix issue with v-list-item--active on both home and active page
        {
            path: '/',
            redirect: { name: 'lotto' }
        },        
        {
            path: '/lotto',
            name: 'lotto',
            component: lotto
        },
        {
            path: '*',
            component: PageNotFound
        }
        // {
        //     path: '/inventory',
        //     name: 'inventory',
        //     component: Inventory
        // },
        // {
        //     path: '/inventory/edit',
        //     name: 'inventoryEdit',
        //     component: InventoryEdit,
        //     beforeEnter: (to, from, next) => {
        //         function isValid (param) {
        //            if (param === 'undefined') false
        //            else true
        //         }
        //         if (!isValid(to.params.name)) {
        //             next({ name: 'inventory' })
        //         }
        //         next()
        //     }
        // },
        // {
        //     path: '/navigation',
        //     name: 'navigation',
        //     component: Navigation
        // },
        // {
        //     path: '/navigation/edit',
        //     name: 'navigationEdit',
        //     component: NavigationEdit,
        //     beforeEnter: (to, from, next) => {
        //         function isValid (param) {
        //            if (param === 'undefined') false
        //            else true
        //         }
        //         if (!isValid(to.params.name)) {
        //             next({ name: 'navigation' })
        //         }             
        //         next()
        //     }
        // },
        // {
        //     path: '/tools',
        //     name: 'tools',
        //     component: Tools
        // },
        
    ]
}