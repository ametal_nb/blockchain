import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        version: '1.1.0',
        api: {
            base: process.env.NODE_ENV == 'production' ? 'https://generator.rapid4cloud.com/' : 'http://localhost:3000/',
            // version: 'version',
            // module: 'module',
            // inventory: 'inventory',
            // inventoryCreate: 'inventory/create',
            // inventoryDetails: 'inventory/details',
            // inventoryScope: 'inventory/scope',
            // inventoryEdit: 'inventory/edit',
            // inventoryAutoFix: 'inventory/autofix',
            // download: 'download',
            // dowloadFile: 'download/byFile',
            // navigation: 'navigation',
            // navigationCreate: 'navigation/create',
            // navigationDetails: 'navigation/details',
            // navigationScope: 'navigation/scope',
            // navigationEdit: 'navigation/edit',
            // navigationAutoFix: 'navigation/autofix',
            // navigationPreview: 'navigation/preview',
            main: '',
            generate: 'generate',
            accounts: 'getAccounts',
            save: 'save'
        },
        // oracleVersion: {
        //     loading: false,
        //     selected: '',
        //     list: [],
        // },
        // oracleModule: {
        //     loading: false,
        //     selected: '',
        //     list: [],
        // },
        // inventory: {
        //     loading: false,
        //     list: []
        // },
        // navigation: {
        //     loading: false,
        //     list: []
        // },
        accounts: {
            loading: false,
            selected: '',
            list: []
        }
    },
    mutations: {
        // updateOracleVersion(state, payload) {
        //     state.oracleVersion = {
        //         loading: payload.loading === undefined ? state.oracleVersion.loading : payload.loading,
        //         selected: payload.selected === undefined ? state.oracleVersion.selected : payload.selected,
        //         list: payload.list === undefined ? state.oracleVersion.list : payload.list,
        //     }
        // },
        updateOracleVersion(state, payload) {
            state.accounts = {
                loading: payload.loading === undefined ? state.accounts.loading : payload.loading,
                selected: payload.selected === undefined ? state.accounts.selected : payload.selected,
                list: payload.list === undefined ? state.accounts.list : payload.list,
            }
        },
        // updateOracleModule(state, payload) {
        //     state.oracleModule = {
        //         loading: payload.loading === undefined ? state.oracleModule.loading : payload.loading,
        //         selected: payload.selected === undefined ? state.oracleModule.selected : payload.selected,
        //         list: payload.list === undefined ? state.oracleModule.list : payload.list,
        //     }
        // },
        // updateInventory(state, payload) {
        //     state.inventory = {
        //         loading: payload.loading === undefined ? state.inventory.loading : payload.loading,
        //         list: payload.list === undefined ? state.inventory.list : payload.list,
        //     }
        // },
        // updateNavigation(state, payload) {
        //     state.navigation = {
        //         loading: payload.loading === undefined ? state.navigation.loading : payload.loading,
        //         list: payload.list === undefined ? state.navigation.list : payload.list,
        //     }
        // },
         updateAccounts(state, payload) {
            state.accounts = {
                loading: payload.loading === undefined ? state.accounts.loading : payload.loading,
                list: payload.list === undefined ? state.accounts.list : payload.list,
            }
        }
    },
    actions: {
        loadAccountsList({ commit, state }) {
            commit('updateAccounts', { loading: true })
            axios.get(`${state.api.base}${state.api.accounts}`)
            .then((response) => {
                console.log('get Account >> ',response.data)
                commit('updateAccounts', { list: response.data.list })
            })
            .catch((error) => {
                // TODO Write in a log file
                // eslint-disable-next-line
                console.error('Error when loading Accounts List', error.message);
            })
            .finally(() => {
                commit('updateAccounts', { loading: false })
            })
        },
        // loadOracleVersionList({ commit, state }) {
        //     commit('updateOracleVersion', { loading: true })
        //     axios.get(`${state.api.base}${state.api.version}`)
        //     .then((response) => {
        //         commit('updateOracleVersion', { list: response.data.list })
        //     })
        //     .catch((error) => {
        //         // TODO Write in a log file
        //         // eslint-disable-next-line
        //         console.error('Error when loading Oracle Version List', error.message);
        //     })
        //     .finally(() => {
        //         commit('updateOracleVersion', { loading: false })
        //     })
        // },
        // loadOracleModuleList({ commit, state }) {
        //     commit('updateOracleModule', { loading: true })
        //     axios.get(`${state.api.base}${state.api.module}?version=${state.oracleVersion.selected}`)
        //     .then((response) => {
        //         commit('updateOracleModule', { list: response.data.list })
        //     })
        //     .catch((error) => {
        //         // TODO Write in a log file
        //         // eslint-disable-next-line
        //         console.error('Error when loading Oracle Module List', error.message);
        //     })
        //     .finally(() => {
        //         commit('updateOracleModule', { loading: false })
        //     })
        // },
        // loadInventoryList({ commit, state }) {
        //     commit('updateInventory', { loading: true })
        //     axios.get(`${state.api.base}${state.api.inventory}?version=${state.oracleVersion.selected}&module=${state.oracleModule.selected}`)
        //     .then((response) => {
        //         commit('updateInventory', { list: response.data.list })
        //     })
        //     .catch((error) => {
        //         // If error, safer to reset the list
        //         commit('updateInventory', { list: [] })
        //         // TODO Write in a log file
        //         // eslint-disable-next-line
        //         console.error('Error when loading Inventory List', error.message);
        //     })
        //     .finally(() => {
        //         commit('updateInventory', { loading: false })
        //     })            
        // },
        // loadNavigationList({ commit, state }) {
        //     commit('updateNavigation', { loading: true })
        //     axios.get(`${state.api.base}${state.api.navigation}?version=${state.oracleVersion.selected}&module=${state.oracleModule.selected}`)
        //     .then((response) => {
        //         commit('updateNavigation', { list: response.data.list })
        //     })
        //     .catch((error) => {
        //         // If error, safer to reset the list
        //         commit('updateNavigation', { list: [] })
        //         // TODO Write in a log file
        //         // eslint-disable-next-line
        //         console.error('Error when loading Navigation List', error.message);
        //     })
        //     .finally(() => {
        //         commit('updateNavigation', { loading: false })
        //     })            
        // },
        // updateOracleVersionSelected({ dispatch, commit }, payload) {
        //     commit('updateOracleVersion', { selected: payload })
        //     // Reset the selected Module as we are switching Oracle version
        //     commit('updateOracleModule', { selected: '' })
        //     // Update the list of modules
        //     dispatch('loadOracleModuleList')
        // },
        // updateOracleModuleSelected({ dispatch, commit }, payload) {
        //     commit('updateOracleModule', { selected: payload })
        //     if (payload) {
        //         // Update all lists
        //         dispatch('loadInventoryList')
        //         dispatch('loadNavigationList')
        //     }
        // }
    },
    getters: {
        api(state) {
            return state.api
        },
    }
})