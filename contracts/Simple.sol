//pragma solidity ^0.5.16;
pragma solidity >=0.4.0 <0.7.0;

pragma experimental ABIEncoderV2;


contract Simple {

   string public hello = "hello world!";
    
    struct Lotto {
        uint24 lottoNumber; //หมายเลขที่ต้องการซื้อ 
        uint24 lottoPrice; //จำนวนเงินที่ต้องการซื้อ(บาท)
        uint24 prizeRound; //งวดรางวัล
        string accAddress; //address บัญชีในระบบ blockchain ของผู้ซื้อสลาก
    }
    
    struct Prize {
        uint24 prizeNumber; //หมายเลขรางวัล 000
        uint24 prizePrice; //มูลค่ารางวัล 80 ล้านบาท
        uint24 prizeRound; //งวดรางวัล
    }

    mapping (uint24 => Lotto) public lottos; //หมายเลขสลากกินแบ่ง uint24 = accId 0,1,2,3 เรียงลำดับตาม truffle
    mapping (uint24 => Prize) public prizes; //รางวัล uint24 = prizeRound 0,1,2,3 งวดรางวัล

    ////////////////////////////////////////////////////////////////////////////////

    //Log the event about a deposit being made by an address and its amount
    event LogDepositMade(address indexed accountAddress, uint amount);
    // constructor(uint _initialAmount) public {
    //     lottoNumber[msg.sender] = _initialAmount;
    // }
    
    //////////////////////////////////// Test Get Set /////////////////////////////////////
    function get() public view returns (string memory) {
        return hello;
    }
    function set(string memory _hello) public {
        hello = _hello;
    }
    
    /////////////////////////////////// Lotto /////////////////////////////////////////////
    function buyLotto(
        uint24 _accId,
        uint24 _lottoNumber,
        uint24 _lottoPrice,
        uint24 _prizeRound,
        string memory _accAddress
    ) public {
        lottos[_accId] = Lotto({   
            lottoNumber: _lottoNumber,
            lottoPrice: _lottoPrice,
            prizeRound: _prizeRound,
            accAddress: _accAddress
        });
    }


    function viewLottoNumber(uint24 _accId) public view returns (Lotto memory) {
        Lotto memory lotto = lottos[_accId];
        return (lotto); //return (lotto.lottoNumber,lotto.price)
    }

    function viewLottoPrice(uint24 _accId) public view returns (uint24) {
        Lotto memory lotto = lottos[_accId];
        return (lotto.lottoPrice);
    }
    
    function viewPrizeRound(uint24 _accId) public view returns (uint24) {
        Lotto memory lotto = lottos[_accId];
        return (lotto.prizeRound);
    }

    /////////////////////////////////// Prize /////////////////////////////////////////////

    function setPrizeNumber(
        uint24 _prizeId,
        uint24 _prizeNumber,
        uint24 _prizePrice,
        uint24 _prizeRound
    ) public {
        prizes[_prizeId] = Prize({   
            prizeNumber: _prizeNumber,
            prizePrice: _prizePrice,
            prizeRound: _prizeRound
        });
    }
    function viewPrizeNumber(uint24 _prizeId) public view returns (uint24) { //prizeId = prizeRound
        Prize memory prize = prizes[_prizeId]; 
        return (prize.prizeNumber); //return (prize.prizeNumber,prize.prizePrize,prize.prizeRound)
    }
    function viewPrizePrice(uint24 _prizeId) public view returns (uint24) { //prizeId = prizeRound
        Prize memory prize = prizes[_prizeId];
        return (prize.prizePrice); 
    }

    /////////////////////////////////// Find Winner  /////////////////////////////////////////////
    function findWinner(uint24 _prizeRound) public view returns (string memory) { //prizeId = prizeRound
        uint24 prizeNumber = prizes[_prizeRound].prizeNumber; //หมายเลขรางวัลในงวดที่ _prizeRound 
        for(uint8 i=0; i<= 9; i++){ //sample account in truffle has maximum 10 accounts
            if(lottos[i].lottoNumber == prizeNumber){
                return lottos[i].accAddress;
            }
        }
        return 'No Winner';
    }

    uint8 private clientCount;
    mapping (address => uint) private balances;
    address public owner;

  // Log the event about a deposit being made by an address and its amount
  //  event LogDepositMade(address indexed accountAddress, uint amount);

    // Constructor is "payable" so it can receive the initial funding of 30, 
    // required to reward the first 3 clients
    constructor() public payable {
        // require(msg.value == 30 ether, "30 ether initial funding required");
        /* Set the owner to the creator of this contract */
        owner = msg.sender;
        clientCount = 0;
    }

    /// @notice Enroll a customer with the bank, 
    /// giving the first 3 of them 10 ether as reward
    /// @return The balance of the user after enrolling
    // function enroll() public returns (uint) {
    //     if (clientCount < 3) {
    //         clientCount++;
    //         balances[msg.sender] = 10 ether;
    //     }
    //     return balances[msg.sender];
    // }

    /// @notice Deposit ether into bank, requires method is "payable"
    /// @return The balance of the user after the deposit is made
    function deposit() public payable returns (uint) {
        balances[msg.sender] += msg.value;
        emit LogDepositMade(msg.sender, msg.value);
        return balances[msg.sender];
    }

    /// @notice Withdraw ether from bank
    /// @return The balance remaining for the user
    function withdraw(uint withdrawAmount) public returns (uint remainingBal) {
        // Check enough balance available, otherwise just return balance
        if (withdrawAmount <= balances[msg.sender]) {
            balances[msg.sender] -= withdrawAmount;
            msg.sender.transfer(withdrawAmount);
        }
        return balances[msg.sender];
    }

    /// @notice Just reads balance of the account requesting, so "constant"
    /// @return The balance of the user
    function balance() public view returns (uint) {
        return balances[msg.sender];
    }

    /// @return The balance of the Simple Bank contract
    function depositsBalance() public view returns (uint) {
        return address(this).balance;
    }

}
